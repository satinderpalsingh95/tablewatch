//
//  ViewController.swift
//  tableWatch
//
//  Created by Satinder pal Singh on 2019-10-30.
//  Copyright © 2019 Satinder pal Singh. All rights reserved.
//

import UIKit
import WatchConnectivity
class ViewController: UIViewController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    @IBAction func buttonPressed(_ sender: Any) {
        if (WCSession.default.isReachable) {
            let message = ["toDo":toDoItems] as [String : Any]
            
            WCSession.default.sendMessage(message, replyHandler: nil)
        }
    }
    
    var toDoItems : [String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.toDoItems = ["Fold Laundry", "Clean Kitchen", "Buy Milk"]
        
        if (WCSession.isSupported() == true) {
            //nameLabel.text = "WCSupported!"
            
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else{
            //nameLabel.text = "WC NOT supported!"
        }
        
        
        
        // Do any additional setup after loading the view.
    }


}


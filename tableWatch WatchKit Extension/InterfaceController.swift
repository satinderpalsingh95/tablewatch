//
//  InterfaceController.swift
//  tableWatch WatchKit Extension
//
//  Created by Satinder pal Singh on 2019-10-30.
//  Copyright © 2019 Satinder pal Singh. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    @IBOutlet weak var table: WKInterfaceTable!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if (WCSession.isSupported() == true) {
            //nameLabel.setText("WC enabled")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            //nameLabel.setText("WC NOT supported!")
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        var toDoItems = message["toDo"] as? [String]
        table.setNumberOfRows(toDoItems!.count, withRowType: "rowIdentifier")
            
        for (index, toDoItem) in toDoItems!.enumerated() {
                if let rowController = table.rowController(at: index) as? ToDoRowController
                {
                    rowController.toDoLabel.setText(toDoItem)
                }
            }
        
        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
